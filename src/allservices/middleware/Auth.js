import MainBase from "@/allservices/controller/MainBase.js";

export default{
    name: "Auth",

    ReaderCategory(options){
        this.OnProgressBar();
        if(options.to.meta.hasOwnProperty('middleware')){
            if(options.to.meta.middleware && MainBase.readerLoggedIn()){
                return options.next();
            }else{
                this.OffProgressBar();
                return options.next('/');
            }
        }else{
            MainBase.logoutUser();
            return options.next(options.from.fullPath);
        }
    },
    GuestCategory(options){
        this.OnProgressBar();
        /*
        if(AuthApi.customerLoggedIn()){
            options.next(options.from.fullPath);
        }else{
            options.next();
        }*/
    },

    PublisherCategory(options){

    },

    ReaderPublisherCategory(options){
        if(options.to.meta.hasOwnProperty('middleware')){
            if(MainBase.readerLoggedIn() || MainBase.publisherLoggedIn()){
                options.next();
            }else{
                options.next('/');
            }
        }
    },

    AllCategory(options){
        if(options.to.meta.hasOwnProperty('middleware')){
            if(MainBase.readerLoggedIn() || MainBase.publisherLoggedIn() || MainBase.guestUser()){
                options.next();
            }else{
                options.next('/');
            }
        }
    },
    OnProgressBar(){
        MainBase.showProgressBar();
        //alert("i am on, progress bar");
    },

    OffProgressBar(){
        MainBase.removeProgressBar();
        //alert("i am on, progress bar");
    }
};