import SendService from "@/allservices/controller/SendService.js";
//import MainBase from "@/allservices/controller/MainBase.js";

export default{
    name: "GuestBase",
    SendService,
    //MainBase
//
    signUp(data){
        return this.SendService.post("front/signup.php", data);
    },

    logIn(data){
        return this.SendService.post("front/login.php", data);
    }

};