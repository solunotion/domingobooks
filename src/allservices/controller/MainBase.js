var os_elem = document.getElementById("onlinestatus");
var online_elem = document.getElementById("onlinestat");
var offline_elem = document.getElementById("offlinestat");
os_elem.style.display = "none";

function removeDiv(){
    setTimeout(function(){
        os_elem.style.display = "none";
    }, 5000);
}

window.addEventListener('online', () => {
    if (os_elem.style.display === "none") {
        os_elem.style.display = "block";
        online_elem.style.display = "block";
        offline_elem.style.display = "none";
        removeDiv();
    }else{
        os_elem.style.display = "block";
        online_elem.style.display = "block";
        offline_elem.style.display = "none";
        removeDiv();;
    }
});
window.addEventListener('offline', ()=>{
    if (os_elem.style.display === "none") {
        os_elem.style.display = "block";
        online_elem.style.display = "none";
        offline_elem.style.display = "block";
        //removeDiv();
    }else{
        os_elem.style.display = "block";
        online_elem.style.display = "none";
        offline_elem.style.display = "block";
        //removeDiv();
    }
});

import SendService from "@/allservices/controller/SendService.js";

export default{
    name: "MainBase",
    SendService,

    openModal(data) {
        let elem = document.getElementById(data);
        var instance = M.Modal.getInstance(elem);
        instance.open();
    },

    closeModal(data){
        let elem = document.getElementById(data);
        var instance = M.Modal.getInstance(elem);
        instance.close();
    },

    userType(){
        if(localStorage.getItem("usertype") === null){
            return "guest";
        }
        if(localStorage.getItem("usertype") === "guest"){
            return "guest";
        }
        if(localStorage.getItem("usertype") === "reader"){
            return "reader";
        }
        if(localStorage.getItem("usertype") === "publisher"){
            return "publisher";
        }
    },

    setSessionItem(key, value){
        localStorage.setItem(key, value);
    },

    getSessionItem(key){
        return JSON.parse(localStorage.getItem(key));
    },

    removeSessionItem(key){
        if(localStorage.removeItem(key)){
            return true;
        }
    },

    clearSession(){
        localStorage.clear();
        window.location.reload();
        
    },

    logoutUser(){
        this.clearSession();
        //return true;
    },

    scrollToTop(){
        window.scroll({
            top: 0,
            left: 0,
            behavior: 'smooth'
        });
    },

    guestUser(){
        if(this.userType() == "guest"){
            return true;
        }else{
            return false;
        }
    },

    readerLoggedIn(){
        if(this.userType() == "reader"){
            return true;
        }else{
            return false;
        }
    },
    
    publisherLoggedIn(){
        if(this.userType() == "publisher"){
            return true;
        }else{
            return false;
        }
    },
    clearIntervalTime(){
        return 580000;
    },
    updateIntervalTime(){
        return 120000;
    },
    alert(type, message){
        if(type == "success"){
            swal({
                title: "Yea, Success",
                text: message,
                icon: "success"
            });
        }

        if(type == "error"){
            swal({
                title: "Nah...",
                text: message,
                icon: "error"
            });
        }
        
        if(type == "info"){
            swal({
                title: "Hello",
                text: message,
                icon: "info"
            });
        }

        if(type == "warning"){
            swal({
                title: "Ahhh",
                text: message,
                icon: "warning"
            });
        }
    },

    myConsole(string, data){
        //console.log(string, data);
    },

    checkCreateBook(data){
        return this.SendService.post("user/checkbook.php", data);
    },
    cancelPublish(data){
        return this.SendService.post("user/cancelbook.php", data);
    },

    userReadBook(data){
        return this.SendService.post("user/readbook.php", data);
    },

    createBook(data){
        return this.SendService.post("common/createbook.php", data);
    },

    withdrawEarnings(data){
        return this.SendService.post("user/withdrawearnings.php", data);
    },

    updateEarnings(data){

    },

    updateWallet(data){

    },

    viewStore(data){
        return this.SendService.post("front/store.php", data);
    },

    myReads(data){
        return this.SendService.post("user/myreads.php", data);
    },

    myProfile(data){
        return this.SendService.post("user/profile.php", data);
    },
    
    myBooks(data){
        return this.SendService.post("user/mybooks.php", data);
    },

    getBanks(data){
        return this.SendService.post("user/getbanks.php", data);
    },

    popularBooks(data){
        return this.SendService.post("front/popular.php", data);
    },

    publishBook(data){
        return this.SendService.post("common/publish.php", data);
    },
    fundWallet(t_data){
        var handler = PaystackPop.setup({
            key: t_data.pspk,
            email: t_data.email,
            amount: (t_data.amount * 100),
            currency: "NGN",
            ref: ''+Math.floor((Math.random() * 1000000000) + 1), // generates a pseudo-unique reference. Please replace with a reference you generated. Or remove the line entirely so our API will generate one for you
            metadata: {
                custom_fields: [
                {
                    display_name: "User",
                    variable_name: "userid",
                    value: t_data.id
                }
                ]
            },
            callback: function(response){
                swal({
                    title: "Yea...",
                    text: "Transaction Successful, you can now proceed",
                    icon: "success"
                });
                localStorage.setItem("t_reference", response.reference);
                var pro_d = JSON.parse(localStorage.getItem("process_data"));
                if(pro_d){
                    if(pro_d.hasOwnProperty("balance")){
                        let old_b = pro_d.balance;
                        let new_b = (Number(old_b)) + (Number(t_data.amount));

                        pro_d.balance = new_b;
                        localStorage.setItem("process_data", JSON.stringify(pro_d));
                    }
                }
                var elem = document.getElementById("progressbar");
                if (elem.style.display === "none") {
                    elem.style.display = "none";
                } else {
                    elem.style.display = "none";
                }
                let elems = document.getElementById("add_deposit");
                var instance = M.Modal.getInstance(elems);
                instance.close();
            },
            onClose: function(){
                var elem = document.getElementById("progressbar");
                if (elem.style.display === "none") {
                    elem.style.display = "none";
                } else {
                    elem.style.display = "none";
                }
            }
        });
        handler.openIframe();
    },
    getWallet(data){
        return this.SendService.post("user/wallet.php", data);
    },
    buyBook(data){
        return this.SendService.post("common/buybook.php", data);
    },

    verifyPay(data){
        return this.SendService.post("user/verifypay.php?reference="+data, data);
    },

    getCatLang(data){
        return this.SendService.post("front/catlang.php", data);
    },

    onlineStatus(){
        var os_elem = document.getElementById("onlinestatus");
        var online_elem = document.getElementById("onlinestat");
        var offline_elem = document.getElementById("offlinestat");

        if(window.navigator.onLine){
            return true;
        }else{
            if (os_elem.style.display === "none") {
                os_elem.style.display = "block";
                online_elem.style.display = "none";
                offline_elem.style.display = "block";
                //removeDiv();
            }else{
                os_elem.style.display = "block";
                online_elem.style.display = "none";
                offline_elem.style.display = "block";
                //removeDiv();
            }
            return false;
        }
    },

    showProgressBar(){
        var elem = document.getElementById("progressbar");
        if (elem.style.display === "none") {
            elem.style.display = "block";
        }else{
            elem.style.display = "block";
        }
    },

    removeProgressBar(){
        var elem = document.getElementById("progressbar");
        if (elem.style.display === "none") {
            elem.style.display = "none";
        } else {
            elem.style.display = "none";
        }
    }
};