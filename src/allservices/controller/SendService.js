//import ApiService from "@/allservices/controller/api/localApi.js";
import ApiService from "@/allservices/controller/api/api.js";

export default{
    name: "SendService",
    ApiService,

    post(string, data){
        if(string == "common/createbook.php"){
            return this.ApiService.postimg(string, data);
        }else{
            return this.ApiService.post(string, data);
        }
    }
};