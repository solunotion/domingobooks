import successJson from "@/json/localJson/successJson.json";
import errorJson from "@/json/localJson/errorJson.json";
import dbJson from "@/json/localJson/dbJson.json";

export default{
    name: "LocalApi",
    successJson, errorJson, dbJson,

    post(string, data){
        if(string == "front/signup.php"){
            let response = Promise.resolve(this.successJson.guest.signup);
            return response;
        }

        if(string == "front/login.php"){
            let response = Promise.resolve(this.successJson.guest.login);
            return response;
        }

        if(string == "user/checkbook.php"){
            let response = Promise.resolve(this.successJson.reader.checkbook);
            return response;
        }

        if(string == "user/createbook.php"){
            let response = Promise.resolve(this.successJson.reader.createbook);
            return response;
        }
        if(string == "user/publish.php"){
            let response = Promise.resolve(this.successJson.reader.publish);
            return response;
        }
        if(string == "user/withdrawearnings.php"){
            let response = Promise.resolve(this.successJson.publisher.withdraw);
            return response;
        }

        if(string == "user/myreads.php"){
            let response = Promise.resolve(this.successJson.reader.myreads);
            return response;
        }

        if(string == "user/mybooks.php"){
            let response = Promise.resolve(this.successJson.publisher.mybooks);
            return response;
        }

        if(string == "front/store.php"){
            let response = Promise.resolve(this.successJson.guest.storebooks);
            return response;
        }

        if(string == "user/profile.php"){
            let response = Promise.resolve(this.successJson.reader.profile);
            return response;
        }

        if(string == "user/getbanks.php"){
            let response = Promise.resolve(this.successJson.reader.banks);
            return response;
        }

        if(string == "front/popular.php"){
            let response = Promise.resolve(this.successJson.guest.popularbooks);
            return response;
        }

        if(string == "front/catlang.php"){
            let response = Promise.resolve(this.successJson.guest.catlang);
            return response;
        }
    },

    postimg(string, data){
        if(string == "user/createbook.php"){
            let response = Promise.resolve(this.successJson.reader.createbook);
            return response;
        }
    }
}