import axios from 'axios';
import MainBase from "@/allservices/controller/MainBase.js";

export default{
    name: "ApiRequest",
    w_platform: "web",
    defaultURL: 'http://api.nairaman.com/api/',
    
    post(string, data){
        if(MainBase.onlineStatus()){
            if((data) && (string)){
                return this.proceed(string, data);
            }
        }
    },

    postimg(string, data){
        if(MainBase.onlineStatus()){
            if((data) && (string)){
                return this.proceedimg(string, data);
            }
        }
    },

    get(string, data){
        if(MainBase.onlineStatus()){
            if((data) && (string)){
                return this.proceedget(string, data);
            }
        }
    },

    proceed(string, data){
        
        return axios({
            method: 'post',
            url:string,
            baseURL: this.defaultURL,
            data: data,
            headers:{
                'Content-Type': 'multiplart/form-data'
            }
        })
        .then(response=>{
            return response.data;
        });
    },
    proceedget(string, data){
        return axios({
            method: 'get',
            url:string,
            baseURL: this.defaultURL,
            data: data,
            headers:{
                'Content-Type': 'multiplart/form-data'
            }
        })
        .then(response=>{
            return response.data;
        });
    },

    proceedimg(string, data){
        return axios({
            method: 'post',
            url:string,
            baseURL: this.defaultURL,
            data: data,
            headers:{
                'Content-Type': 'multiplart/form-data'
            }
        })
        .then(response=>{
            return response.data;
        });
    }
};