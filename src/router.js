import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Auth from "./allservices/middleware/Auth.js";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/bookinfo",
      name: "bookinfo",
      component: () =>
        import("./views/BookInfo.vue")
    },
    {
      path: "/userdashboard",
      name: "userdashboard",
      beforeEnter: (to, from, next) => {
        let options = {to, from, next};
        Auth.ReaderCategory(options);
      },
      meta:{
        middleware: true
      },
      component: () =>
        import("./views/UserDashboard.vue")
    },
    {
      path: "/mypublished",
      name: "mypublished",
      beforeEnter: (to, from, next) => {
        let options = {to, from, next};
        Auth.ReaderCategory(options);
      },
      meta:{
        middleware: true
      },
      component: () =>
        import("./views/MyPublished.vue")
    },
    {
      path: "/profile",
      name: "profile",
      beforeEnter: (to, from, next) => {
        let options = {to, from, next};
        Auth.ReaderCategory(options);
      },
      meta:{
        middleware: true
      },
      component: () =>
        import("./views/Profile.vue")
    },
    {
      path: "/bookstore",
      name: "bookstore",
      beforeEnter: (to, from, next) => {
        let options = {to, from, next};
        Auth.AllCategory(options);
      },
      meta:{
        middleware: true
      },
      component: () =>
        import("./views/BookStore.vue")
    },
    {
      path: "/publishbook",
      name: "publishbook",
      beforeEnter: (to, from, next) => {
        let options = {to, from, next};
        Auth.ReaderCategory(options);
      },
      meta:{
        middleware: true
      },
      component: () =>
        import("./views/PublishBook.vue")
    },
    {
      path: "/about",
      name: "about",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/About.vue")
    }
  ]
});