import Vue from "vue";
import './plugins/axios'
import App from "./App.vue";
import router from "./router";
import store from "./store";

Vue.config.productionTip = false;
Vue.prototype.$appName = "DomingoBooks";
Vue.prototype.$defaultURL =  "http://api.nairaman.com/api/";
Vue.prototype.$waitingText = "Please Wait... if this stays too long ? check your network connection and try again.";
Vue.prototype.$appCurrency = "N";

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
